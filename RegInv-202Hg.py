#!/usr/bin/env python3
# -*- coding: utf-8 -*-
##############################################################################
###                       RegInv-202Hg                                     ###
###  Alternating regularized inversion of the 202Hg to 198Hg isotopic      ###
### ratios (d202Hg) and fractional amounts of unique Hg chemical species   ### 
###                Romain Brossier, Alain Manceau                          ###
###                          v1.0 05/2021                                  ###
##############################################################################


import matplotlib.pyplot as plt
import pandas as pd
import numpy as np
from mpl_toolkits.mplot3d import Axes3D
import matplotlib.tri as tri
import matplotlib.patches as mpatches
from numpy.lib import recfunctions as rfn
import datetime


#linear inversion function
def solvelininvprob2(G,Cdinv,dobs,m0,lam):
    Atmp=G.T@Cdinv@G
    scale=np.max(Atmp)
   # print(scale,np.shape(scale))
    A=Atmp+lam*scale*np.eye(len(G.T))
    b=G.T@Cdinv@dobs+lam*m0
    mopt=np.linalg.solve(A,b)
    Cm=np.linalg.inv(G.T@Cdinv@G)
    return mopt,Cm


###############################
# management of the output log
###############################
now = datetime.datetime.now()

print("")
print("***********************")
print("     RegInv-202Hg      ")
print("***********************")
print ("run launched : ")
print (now.strftime("%H:%M:%S %d/%m/%Y "))
print("")
print("Calculation of the species-specific d202Hg values and fractional amounts of the Hg species by alternating regularized inversion")
print("")
print("Authors:")
print("Romain Brossier (romain.brossier@univ-grenoble-alpes.fr)")
print("Alain Manceau (alain.manceau@ens-lyon.fr)")
print("")
print("Program Version : 1.0")
print("Release date : 15 Mai 2021")
print("")
print("The mathematical formalism can be found here:")
print("A. Manceau, R. Brossier, B. A. Poulin (2021) ACS Earth and Space Chemistry:DOI:  https://doi.org/10.1021/acsearthspacechem.1c00082")
print("The Python source can be found here:  => https://gricad-gitlab.univ-grenoble-alpes.fr/mercury_inversion/alternating_inversion ")
print("")

#############################
# management of the inputs
#############################

print("***********************")
#input file name in csv format
file=str(input("input file name in csv format\n"))
print("file name is", file)
#print("assumed separator of csv in ;")
nheader=int(input("number of header lines in file\n"))
#nlines=int(input("number of lines of data to be read in file\n"))
#print("the assumed csv format is")
#print("header line 1")
#print(".....")
#print("header line n")
#print("sample ID1 ; sample name or description ; d202Hg ; Std d202Hg ; %MeHg ; icode ")
#print("sample ID2 ; sample name or description ; d202Hg ; Std d202Hg ; %MeHg ; icode ")
#print(".....")
#print("sample IDm ; sample name or description ; d202Hg ; Std d202Hg ; %MeHg ; icode ")
#print("")
print("management of initial/boundary conditions")
print("")
d202MeHg=float(input("initial value, for inversion, for d202MeHg (should be around 1.5) \n"))
print("")
d202HgSec4=float(input("initial value, for inversion, for d202HgSec4 (should be around -1.5) \n"))
print("")
d202HgSe=float(input("initial value, for inversion, for d202HgSe (should be around 0) \n"))
print("")
print("inversion management")
tolrange=1.
tolrange=float(input("tolerance on the sum of percentage of species (advised value is around 1 to 5 %)\n"))
tolrange=0.01*tolrange #put to fraction
lambda_inv=1e-2
lambda_inv=float(input("weight of the regularization term in inversion (1e-2 is a good practical value)\n"))




#############################
# READ INPUT FILE
#############################
#dfd=pd.read_csv(file,sep=';',header=None,nrows=nlines+nheader,comment='#')
dfd=pd.read_csv(file,sep=';',header=None,comment='#')
dfd=dfd.fillna(0)
#print(dfd)

#############################
# DO THE REAL WORK NOW
#############################
#index to start the data in the dfd pandas structure
istart=nheader
#extract the data part
dfd_data=np.array(dfd.loc[istart:,2:4].astype(float))
dfd_icode=np.array(dfd.loc[istart:,5].astype(int))

#fill the observation vector fill in with d202Hg
dobs=np.array([dfd_data[:,0]]).T
#print(dobs,np.shape(dobs))
#fill the standard deviation vector
stddobs=np.array([dfd_data[:,1]]).T
#print(stddobs,np.shape(stddobs))
#fill the G table now with input
G=0.01*np.concatenate((np.array([dfd_data[:,2]]).T,np.array([100.-dfd_data[:,2]]).T),axis=1)
Cdinv=np.diagflat(stddobs**(-2))


#Now go to a third dimension problem
Gstep2=np.zeros((len(G),3))
#and fill with 0
Gstep2[:,0]=G[:,0]
#initialize the solution with initial guess
mstep2=[d202MeHg,d202HgSec4,d202HgSe]#-0.15 comes from the minimum MeHg sample

#m0 is used for regularization
m0=np.zeros((3,1))


modelnormback=np.sqrt(mstep2[0]**2+mstep2[1]**2+mstep2[2]**2)

#loop over the iterations of the alternating inversion
for iter in range(0,10000):
    #first step: compute the species fraction with the current "plane" defined by mstep2
    for i in  range(0,len(dobs)):
        # Gstep2[i,0] is provided in input for MeHg -> do not touch it
        # Gstep2[i,1] and Gstep2[i,2] are given by a simple 2x2 linear system:
        Gstep2[i,2]=(dobs[i,0]-mstep2[0]*Gstep2[i,0]-(1-Gstep2[i,0])*mstep2[1])/(mstep2[2]-mstep2[1])
        Gstep2[i,1]=1.-Gstep2[i,0]-Gstep2[i,2]
    #in case icode=3 ->  Gstep2[i,2]=0    
    for i in range(0,len(dobs)):
        if dfd_icode[i] == 3 :  #force HgSe to be 0 
            #print(i)
            Gstep2[i,1]=1.-Gstep2[i,0]
            Gstep2[i,2]=0
    #in case icode=5 ->  Gstep2[i,1]=0    
    for i in range(0,len(dobs)):
        if dfd_icode[i] == 5 :  #force HgSec4 to be 0 
            #print(i)
            Gstep2[i,2]=1.-Gstep2[i,0]
            Gstep2[i,1]=0
    #in case icode=6 ->  Gstep2[i,0]=0    for MeHg
         
    #now truncate the negative values and recompute the other part
    for i in range(0,len(dobs)):
        if Gstep2[i,1] < 0 and np.abs(Gstep2[i,1]) > tolrange:
            Gstep2[i,1]=-tolrange
            Gstep2[i,2]=1.-Gstep2[i,0]-Gstep2[i,1]
        if Gstep2[i,2] < 0 and np.abs(Gstep2[i,2]) > tolrange:
            Gstep2[i,2]=-tolrange
            Gstep2[i,1]=1.-Gstep2[i,0]-Gstep2[i,2]

    #now solve the regularized linear inverse problem to find the new mopt
    mopt,Cm=solvelininvprob2(Gstep2,Cdinv,dobs,m0,lambda_inv)
    #fill the vector with the solution
    mstep2=[mopt[0,0],mopt[1,0],mopt[2,0]]
    #compute the norm
    modelnormnew= np.sqrt(np.sum(mopt**2))
    #print(iter,np.abs(modelnormnew-modelnormback),mstep2)
    #check convergence
    if np.abs(modelnormnew-modelnormback) < 1e-8:
        #print('iter',iter)
        break
    modelnormback=modelnormnew

#compute the predicted d202Hg by the model    
dcal=mopt[0,0]*Gstep2[:,0]+mopt[1,0]*Gstep2[:,1]+mopt[2,0]*Gstep2[:,2]


#############################
# WRITE OUTPUTS 
#############################
print("")
now = datetime.datetime.now()      
print(" end of computation done ")
print (now.strftime("%H:%M:%S %d/%m/%Y "))
print("***********************")
print(" INPUTS")
print("***********************")
print("input file name",file)
print("initial guess of d202MeHg",d202MeHg)
print("initial guess of d202Hg(Sec)4",d202HgSec4)
print("initial guess of d202HgSe",d202HgSe)
print("tolerance on the sum of percentage of species",tolrange/0.01,'%')
print("weight of the regularization term in inversion",lambda_inv)
print("")
print("content of the input file read")
print(dfd.to_string())
#print(dfd)
print("")
print("***********************")
print(" OUTPUTS")
print("***********************")
print("")
print("Number of iterations:", iter)
print("solution for d202MeHg =",mopt[0,0])
print("solution for d202Hg(Sec)4=",mopt[1,0])
print("solution for d202HgSe=",mopt[2,0])
#add data in the dfd pandas frame
tmp=np.around(100.*Gstep2[:,1],1)
header=[0] * nheader
header[-1]='%Hg(Sec)4'
data_out=header+tmp.tolist()
dfd['6'] = data_out
tmp=np.around(100.*Gstep2[:,2],1)
header[-1]='%HgSe'
data_out=header+tmp.tolist()
dfd['7'] = data_out
tmp=np.around(dcal, 2)
header[-1]='d202Hg cal'
data_out=header+tmp.tolist()
dfd['8'] = data_out
pd.options.display.float_format = '{:,.2f}'.format #option for 2 digits printing
print(dfd.to_string())
#print(dfd)
print('end of output')


#############################
# OUTPUT 3D VIEW  
#############################

name=['%MeHg', '%Hg(Sec)4', '%HgSe']

b=np.array([[0.5,np.sqrt(3)/2]])
a=np.array([[0,0]])
c=np.array([[1,0]])
summits=np.concatenate((a,b,c,a),axis=0)
fig = plt.figure(2)
ax = fig.gca(projection='3d')

ax.set_xticklabels([])
ax.set_yticklabels([])
ax.zaxis.set_rotate_label(False)  # disable automatic rotation
ax.set_zlabel('$\u03B4^{202}$Hg(\u2030)',rotation=90)

Gbarycentric=np.zeros((len(Gstep2),2))
Gbarycentric[:,0]=Gstep2[:,0]*a[0,0]+Gstep2[:,1]*b[0,0]+Gstep2[:,2]*c[0,0]
Gbarycentric[:,1]=Gstep2[:,0]*a[0,1]+Gstep2[:,1]*b[0,1]+Gstep2[:,2]*c[0,1]
ax.scatter(Gbarycentric[:,0], Gbarycentric[:,1], dobs, c='c', marker="v", alpha=1)

tmp=np.concatenate((mopt[:,0],[mopt[0,0]]),axis=0)
ax.text(a[0,0],a[0,1]+0.1,mopt[0,0], name[0],color="red")
ax.text(b[0,0],b[0,1],mopt[1,0], name[1],color="blue" )
ax.text(c[0,0],c[0,1],mopt[2,0]+0.4, name[2],color="green" )
ax.plot(summits[:,0],summits[:,1],tmp,color='c' )
ax.view_init(elev=12, azim=53)
plt.savefig('image.eps', format='eps')
plt.show()



