# alternating_inversion

## presentation

The Python code of the RegInv-202Hg software calculates the isotopic ratio of 202Hg to 198Hg (d202Hg) and fractional amounts of unique Hg chemical species by alternating regularized inversion.

Authors: Romain Brossier (romain.brossier@univ-grenoble-alpes.fr) Alain Manceau (alain.manceau@ens-lyon.fr)

This code is associated with the article A. Manceau, R. Brossier, B. A. Poulin (2021) ACS Earth and Space Chemistry: DOI: https://doi.org/10.1021/acsearthspacechem.1c00082, in which the mathematical formalism is described and applied to data from long-finned pilot whale tissues. The use of this code in publications should reference the original article.

---------------------------------
## inputs

The code requires two files, a dataset in .csv format and an input file in ascii which invokes RegInv-202Hg. The input files and datasets used in Manceau et al. (2021) are provided in the Git repository.

### the general structure of the csv file is as follows

header line 1

.....

header line n

sample ID1 ; sample name or description ; d202Hg ; Std d202Hg ; %MeHg ; icode 

sample ID2 ; sample name or description ; d202Hg ; Std d202Hg ; %MeHg ; icode

.....

sample IDm ; sample name or description ; d202Hg ; Std d202Hg ; %MeHg ; icode 


where for each sample

- "sample ID" and "sample name or description" are text strings to identify the sample (just for display purpose)
- "d202Hg" and "Std d202Hg" are floating number related to the measured value of d202Hg and standard deviation for the sample
- "%MeHg" is a floating number related to the percentage of MeHg in the sample

the icode (integer) is a flag which specifies, in a unique way, which species % are known and unknown
- MeHg has a weight of 1 (it should always been considered)
- Hg(Sec)4 has a weight of 2
- HgSe has a weight of 4

The icode is the sum of the three weights. Examples: inverting for the 3 species gives icode=7; inverting for %MeHg and %Hg(Sec)4 gives icode=3; inverting for %MeHg and %HgSe gives icode = 5.

The format follows the .csv standard with ";", commented lines in the data file start with a "#" and are ignored at the reading time

### the general structure of the input file is as follows

1. the name of the .csv file, including its extention
2. the number of header lines (n, integer)
3. the initial species-specific isotopic values. It is recommended to take about 1.5 for MeHg, about -1.5 for Hg(Sec)4, and about 0 for HgSe, to facilitate convergence (float)
4. the tolerance value for the sum of the three species  The sum is allowed to fluctuate slightly above or below 100% during the iterations. 1 to 5 % are good practical values. A value close to 0% may lead to instabilities
5. the weight of regularization. 0.01 is good practical value

In addition to providing an output log, the software creates a 3D image of the f(MeHg), f(Hg(Sec)4), f(HgSe) hyperplane and experimental d202Hg values (vectorial postscript .eps format).


---------------------------------------
## data examples
Two datasets used in Manceau et al. (2021) are provided :
1. A subset of the Li et al. (2020) data:  Li_et_al_2020_dataset.csv
2. A subset of the Bolea-Fernandez et al. (2019) data: Bolea-Fernandez_et_al_2019_dataset.csv

---------------------------------------

## running examples

1. To reproduce the Manceau et al. (2021) results for the Li et al. (2020) data, launch `python3 RegInv-202Hg.py < li_input.txt` or `python RegInv-202Hg.py < li_input.txt` (if the default python calls python3.X)

2. To reproduce the Manceau et al. (2021) results for the the Bolea-Fernandez et al. (2019) data, launch `python3 RegInv-202Hg.py < bolea-fernandez_input.txt` or `python RegInv-202Hg.py < bolea-fernandez_input.txt` (if the default python calls python3.X)

For running the examples while directing the output to a log text file, you can launch
1. `python3 RegInv-202Hg.py < li_input.txt > log_li.txt` or `python RegInv-202Hg.py < li_input.txt > log_li.txt`
2. `python3 RegInv-202Hg.py < bolea-fernandez_input.txt > log_bolea-fernandez.txt` or `python RegInv-202Hg.py < bolea-fernandez_input.txt > log_bolea-fernandez.txt`

